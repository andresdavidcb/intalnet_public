$(document).ready(function () {

	$count=0;$playing=true;
	$lim = $("figure .slide").length;
	
	resizeFig();
	$(window).resize(function(){
		resizeFig();
	})
	$(".prev").click(function(){
		prevSlide();
	})
	$(".next").click(function(){
		nextSlide();
	})
	$(".pause").click(function(){
		if($playing){
			stopInterval();
			$(this).css({"background":"url(img/style/play.png) no-repeat center"});
		}else{
			$(this).css({"background":"url(img/style/pause.png) no-repeat center"});
			$playing=!$playing;
			nextSlide();
		}
	})
	$("nav ul li a").click(function(){
		stopInterval();
	});
	$interval=setInterval(nextSlide,5000);
	
})
function stopInterval(){
	
	clearInterval($interval);
}
function nextSlide(){
	$count=($count+1) % ($lim - 1);
	
	$("figure .slide").each(function(i,el){
		$(el).animate({"left":((i- $count) * $wFig)+"px" });
	})
}
function prevSlide(){
	//stopInterval();
	$count--;//count=0;
	$("figure .slide").each(function(i,el){
		if($count>=0){
			$(el).animate({"left":((i - $count) * $wFig)+"px" });
		}
		else{
			$count=0;
		}
	})

}
function resizeFig(){
	$wFig= ($(window).width()>600)?$(window).width() / 2:$(window).width();
	$hFig= $wFig * (9 / 16) + 30;
	$("figure").css({"height":$hFig});
	$("figure .slide").each(function(i,el){
		$(el).css({"height": + $hFig +"px","width":$wFig+"px","position":"absolute","left":(i * $wFig)+"px"});			
	})	
	$yFig = $("figure .slide").position().top;
	$hBtton = $(".slide img").height();
	$(".prev").css({"height":$hBtton,"width":"64px","position":"absolute","left":"5px","top":$yFig});
	$(".next").css({"height":$hBtton,"width":"64px","position":"absolute","right":"5px","top":$yFig});
	$(".pause, .play").css({"height":$hBtton,"width":"64px","position":"absolute","left":($wFig -32)+"px","top":$yFig});
}